package com.chat.roo.appl.dto;

import java.util.Date;

public class MessageDto {
	
	private String MsgId;
	public String getMsgId() {
		return MsgId;
	}
	public void setMsgId(String msgId) {
		MsgId = msgId;
	}
	public String getMessage_txt() {
		return message_txt;
	}
	public void setMessage_txt(String message_txt) {
		this.message_txt = message_txt;
	}
	public String getUsers_id() {
		return users_id;
	}
	public void setUsers_id(String users_id) {
		this.users_id = users_id;
	}
	public String getChatroom_id() {
		return chatroom_id;
	}
	public void setChatroom_id(String chatroom_id) {
		this.chatroom_id = chatroom_id;
	}
	public Date getDate_time() {
		return date_time;
	}
	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}
	private String message_txt;
	private String users_id;
	private String chatroom_id;
	private Date date_time;
	
	
	

}
