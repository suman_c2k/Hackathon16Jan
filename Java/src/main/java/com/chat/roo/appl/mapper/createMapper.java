package com.chat.roo.appl.mapper;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.chat.roo.appl.dto.MessageDto;
import com.chat.roo.appl.model.Message;


@Component
public class createMapper {
		
		public Message getPlacedOrder(MessageDto messagedto)
		{
			Message createmessage=new Message();
			createmessage.setChatroom_id(messagedto.getChatroom_id());
			createmessage.setDate_time(new Date());
			createmessage.setMessage_txt(messagedto.getMessage_txt());
			createmessage.setMsgId(messagedto.getMsgId());
			createmessage.setUsers_id(messagedto.getUsers_id());
			
			return createmessage;
		}

}
