package com.chat.roo.appl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.chat.roo.appl.model.ChatRoom;
import com.chat.roo.appl.model.Message;


public interface ChatService {
	
	public Iterable<ChatRoom>getAllChat();

	public Iterable<Message> getAllMessage();

	public Message getMessageById(String messageId);

	public Boolean createMessage(Message message);

	
	
	

}
