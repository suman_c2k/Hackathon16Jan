package com.chat.roo.appl.dao;

import java.util.List;

import com.chat.roo.appl.model.ChatRoom;
import com.chat.roo.appl.model.Message;


public interface ChatDao {
	
	public Iterable<ChatRoom> getAllChatInfo();

	public Iterable<Message> getAllMessage();

	public Message getMessageById(String messageId);

	public Boolean createMessage(Message message);

}
