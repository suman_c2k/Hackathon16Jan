package com.chat.roo.appl.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chat.roo.appl.model.ChatRoom;

public interface ChatRepository extends JpaRepository<ChatRoom, String> {
	
	

}
