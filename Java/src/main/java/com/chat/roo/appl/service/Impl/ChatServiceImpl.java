package com.chat.roo.appl.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chat.roo.appl.dao.ChatDao;
import com.chat.roo.appl.model.ChatRoom;
import com.chat.roo.appl.model.Message;
import com.chat.roo.appl.service.ChatService;

@Service
public class ChatServiceImpl implements ChatService {

	@Autowired
	private ChatDao getChatDao;
	@Override
	public Iterable<ChatRoom> getAllChat() {
		
		//List<ChatRoom>chatList=getChatDao.getAllChatInfo();
		
		return getChatDao.getAllChatInfo();
	}
	@Override
	public Iterable<Message> getAllMessage() {
		
		return getChatDao.getAllMessage();
	}
	@Override
	public Message getMessageById(String messageId) {
		
		
		return getChatDao.getMessageById(messageId);
	}
	@Override
	public Boolean createMessage(Message message) {
		// TODO Auto-generated method stub
		return getChatDao.createMessage(message);
	}

}
