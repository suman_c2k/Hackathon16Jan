package com.chat.roo.appl.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chat.roo.appl.model.Message;

public interface CreateMessageRepository extends JpaRepository<Message, Integer> {

}
