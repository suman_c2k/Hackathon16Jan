package com.chat.roo.appl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan (basePackages = {"com.chat.roo.appl.*"})
public class ChatRoomApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(ChatRoomApplication.class, args);
	}
}
