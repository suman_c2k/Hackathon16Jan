package com.chat.roo.appl.getChatInfo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chat.roo.appl.dto.MessageDto;
import com.chat.roo.appl.mapper.createMapper;
import com.chat.roo.appl.model.ChatRoom;
import com.chat.roo.appl.model.Message;
import com.chat.roo.appl.service.ChatService;




@RestController
@RequestMapping(value="/api")

public class ChatController {
	
	@Autowired
	private ChatService chatService;
	
	@Autowired
	private createMapper orderMapper;
	
	@CrossOrigin
	@GetMapping("/getchatroom")
	public Iterable<ChatRoom> getChatDetails()
	{
		//List<ChatRoom> chatList=chatService.getAllChat();
		
		return chatService.getAllChat();
	}
	
	
	@CrossOrigin
	@GetMapping("/getmessage")
	public Iterable<Message>getAllMessage(){
		
		return chatService.getAllMessage();
	}
	
	
	@GetMapping("/getMessageById/{messageId}")
	public Message getMessageById(@PathVariable("messageId")String messageId){
		
		return chatService.getMessageById(messageId);
		
	}
	
	@CrossOrigin
	@PostMapping(value = "/create/createmessage")
	public ResponseEntity<String> placeOrder(@RequestBody MessageDto messagedto){
		
		 Message message=orderMapper.getPlacedOrder(messagedto);
		Boolean status =chatService.createMessage(message);
		
		HttpHeaders header = new HttpHeaders();
		if(status){
			return new ResponseEntity(header, HttpStatus.CREATED);
		}else{
			return new ResponseEntity(header, HttpStatus.EXPECTATION_FAILED);
		}

	}
}

	
	
