package com.chat.roo.appl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="chatroom")
public class ChatRoom {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="chatroom_id")
	private String chatRoomId;
	
	public String getChatRoomId() {
		return chatRoomId;
	}
	public void setChatRoomId(String chatRoomId) {
		this.chatRoomId = chatRoomId;
	}
	@Column(name="chatroom_title")
	private String cahtRoomTitle;
	
	@Column(name="chatroom_des")
	private String chatRoomDesc;
	
	
	public String getCahtRoomTitle() {
		return cahtRoomTitle;
	}
	public void setCahtRoomTitle(String cahtRoomTitle) {
		this.cahtRoomTitle = cahtRoomTitle;
	}
	public String getChatRoomDesc() {
		return chatRoomDesc;
	}
	public void setChatRoomDesc(String chatRoomDesc) {
		this.chatRoomDesc = chatRoomDesc;
	}
	

}
