package com.chat.roo.appl.DaoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.chat.roo.appl.Repository.ChatRepository;
import com.chat.roo.appl.Repository.CreateMessageRepository;
import com.chat.roo.appl.Repository.MessageRepository;
import com.chat.roo.appl.dao.ChatDao;
import com.chat.roo.appl.model.ChatRoom;
import com.chat.roo.appl.model.Message;



@Repository
@PropertySource("classpath:application.properties")
public class ChatDaoImpl implements ChatDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ChatRepository persistOrder;
	
	@Autowired
	private MessageRepository messagerepos;
	
	@Autowired
	private CreateMessageRepository createmessagerepository;
	
	@Override
	public Iterable<ChatRoom> getAllChatInfo() {
		
		/*List<ChatRoom> tempList = entityManager.createQuery(env.getProperty("getChatIno"), ChatRoom.class)
				.getResultList();
		*/
		//List<ChatRoom>listOfChats=persistOrder.findAll();

		return persistOrder.findAll();
	}

	@Override
	public Iterable<Message> getAllMessage() {
		// TODO Auto-generated method stub
		return messagerepos.findAll();
	}

	@Override
	public Message getMessageById(String messageId) {

		return messagerepos.findOne(messageId);
	}

	@Override
	@Transactional
	public Boolean createMessage(Message message) {
		createmessagerepository.saveAndFlush(message);
		return true;
	}

}
