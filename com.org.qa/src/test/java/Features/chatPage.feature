Feature: Chat Box chat Feature
Scenario: Chat Box Chat room selection Scenario
Given user is already logged into page
Then I have option to select Chatroom

Scenario: Chat Box messages in Chat room Scenario
Given user is already logged into page
When Chatroom is selected
Then I can see messages in Chatroom

Scenario: Chat Box load old messages in Chat room Scenario
Given user is already logged into page
When Chatroom is selected
Then I can see messages in Chatroom

Scenario: Chat Box send messages in Chat room Scenario
Given user is already logged into page
When Chatroom is selected
Then I can send messages in Chatroom