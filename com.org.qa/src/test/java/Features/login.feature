Feature: Chat Box Login Feature
Scenario Outline: Chat Box Login Test Scenario
Given user is already on Login Page
When title of login page is ChatBox
Then user enters "<username>" and "<password>"
Then user clicks on login button
Then user is on home page
Then Close the browser
Examples:
	| username | password |
	| abcd     | test@123 |	
	

Scenario Outline: Chat Box Login Test Scenario
Given user is already on Login Page
When title of login page is ChatBox
Then user enters invalid "<username>" and "<password>"
Then user clicks on login button
Then user is on Login page
Then Close the browser
Examples:
	| username | password |
	| abcd     | test@123 |			