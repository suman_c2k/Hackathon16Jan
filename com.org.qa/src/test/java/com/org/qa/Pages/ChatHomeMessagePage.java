package com.org.qa.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.org.qa.Base.TestBaseClass;

public class ChatHomeMessagePage extends TestBaseClass{
	
	@FindBy(id = "")
	WebElement username;

	@FindBy(name = "password")
	WebElement pswd;

	@FindBy(id = "")
	WebElement Login;

	public ChatHomeMessagePage() {
		PageFactory.initElements(driver, this);
	}

	public SearchMessagePage serachMessages() {
		
		return new SearchMessagePage();

	}

}
