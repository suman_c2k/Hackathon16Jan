package com.org.qa.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.org.qa.Base.TestBaseClass;

public class LoginPage extends TestBaseClass {

	@FindBy(id = "")
	WebElement username;

	@FindBy(name = "")
	WebElement pswd;

	@FindBy(id = "")
	WebElement Login;

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	public ChatHomeMessagePage login() {
		
		return new ChatHomeMessagePage();

	}

}
