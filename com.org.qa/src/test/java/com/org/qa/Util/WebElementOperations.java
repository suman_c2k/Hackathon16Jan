package com.org.qa.Util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.qa.Base.TestBaseClass;

public class WebElementOperations extends TestBaseClass {

	// Actions action;
		static int maxNoOfRetries = 5;
		static WebDriverWait eWait= new WebDriverWait(driver, 20); 

		public static void entertextfiedValue(WebElement wef, String txt) throws IOException {
			try {
				eWait.until(ExpectedConditions.visibilityOf(wef));
				wef.sendKeys(txt);

			} catch (Exception e) {

			}
		}

		public static void clickOnWebElement(WebElement wef, String txt) throws IOException {
			try {
				eWait.until(ExpectedConditions.visibilityOf(wef));
				wef.click();

			} catch (Exception e) {

			}
		}
		
		public static void waituntilElementIsAvailable(WebElement element) {
			try {
				 new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(element));

			}

			catch (Exception e) {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				// System.out.println(e.getMessage());

			}
		}

		public static void waitUntilElementIsStable(WebElement element) throws IOException {
			int counter = 0;
			while (maxNoOfRetries > 0 && counter != maxNoOfRetries) {
				try {
					element.isDisplayed();

					break;
				} catch (Exception e) {
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					// System.out.println(e.getMessage());

				}
				counter++;
			}
		}

	
	

}
