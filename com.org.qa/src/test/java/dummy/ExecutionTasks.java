package dummy;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ExecutionTasks {

	static WebDriver driver1, driver2;

	public static void main(String args[]) {

		driver1 = chromeDriver();
		driver2 = fireFoxDriver();

		driver1.get("https://www.google.co.in/");
		driver1.findElement(By.xpath("//input[@name = 'q']")).sendKeys("Chrome browser");
		driver2.get("https://www.google.co.in/");
		driver2.findElement(By.xpath("//input[@name = 'q']")).sendKeys("FireFox browser");

	}

	public static WebDriver chromeDriver() {
		System.setProperty("webdriver.chrome.driver",
				"D:/Hackthon_Chat_Box/Hackathon16Jan/com.org.qa/src/test/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}

	public static WebDriver fireFoxDriver() {
		System.setProperty("webdriver.gecko.driver",
				"D:/Hackthon_Chat_Box/Hackathon16Jan/com.org.qa/src/test/resources/geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		return driver;
	}

}
