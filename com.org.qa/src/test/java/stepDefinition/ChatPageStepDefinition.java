package stepDefinition;

import com.org.qa.Base.TestBaseClass;
import com.org.qa.Pages.ChatHomeMessagePage;
import com.org.qa.Pages.LoginPage;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChatPageStepDefinition extends TestBaseClass{
	
	LoginPage lp;
	ChatHomeMessagePage chp;
	
	public ChatPageStepDefinition() {
		super();
	}
	
	@Given("^user is already logged into page$")
	public void user_is_already_logged_into_page() {
		initialization();
		lp = new LoginPage();
		chp=lp.login();
	}

	@Then("^I have option to select Chatroom$")
	public void i_have_option_to_select_Chatroom()  {
	   
	}

	@When("^Chatroom is selected$")
	public void chatroom_is_selected()  {
	   
	}

	@Then("^I can see messages in Chatroom$")
	public void i_can_see_messages_in_Chatroom() {
	    
	}

	@Then("^I can send messages in Chatroom$")
	public void i_can_send_messages_in_Chatroom()  {
	   
	}


}
