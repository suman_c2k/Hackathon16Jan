package stepDefinition;

import com.org.qa.Base.TestBaseClass;
import com.org.qa.Pages.LoginPage;
import static org.junit.Assert.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginStepDefinition extends TestBaseClass {

	LoginPage lp;

	public LoginStepDefinition() {
		super();
	}

	@Given("^user is already on Login Page$")
	public void user_is_already_on_Login_Page() {
				initialization();
		lp = new LoginPage();
	}

	@When("^title of login page is ChatBox$")
	public void title_of_login_page_is_ChatBox() {
		String title = driver.getTitle();
		System.out.println(title);
		//assertEquals("Google", title);

	}

	@Then("^user enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_and(String arg1, String arg2) {
		System.out.println("then");
	}

	@Then("^user clicks on login button$")
	public void user_clicks_on_login_button() {
		System.out.println("then2");
	}

	@Then("^user is on home page$")
	public void user_is_on_home_page() {
		System.out.println("then3");
	}

	@Then("^Close the browser$")
	 public void close_the_browser(){
		 driver.quit();
	 }
	
	
	@Then("^user enters invalid \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_invalid_and(String arg1, String arg2) {
	   
	}

	@Then("^user is on Login page$")
	public void user_is_on_Login_page() throws Throwable {
	    
	}

	
}
